public class TheatreStaff {
    public static void main(String [] args) {
        String[] employeeNames = {"Alice Dupont", "Bob Martin", "Charlie Besson", "Diane Loriot", "Eva Joly"};
        int[] hoursWorked = {35, 38, 35, 38, 40};
        double[] hourlyRates = {12.5, 15.0, 13.5, 14.5, 13.0};
        String[] positions = {"Caissier", "Projectionniste", "Caissier", "Manager", "Caissier"};

        for (int i = 0; i < employeeNames.length; i++) {
            var currentEmployees = employeeNames[i];    
            var currentHour = hoursWorked[i];
            var currentRate = hourlyRates[i];
            var currentJob = positions[i];

            var overtime = currentHour - 35;
            if (overtime > 0) {
                var salary = 35 * currentRate + overtime * currentEmployees * 1.5;
                System.out.println(currentEmployees + " " + salary);
            } else {
                var salary = currentHour * currentRate;
                System.out.println(currentEmployees + " " + salary);
            }
        }
    }

    String searchPosition = "Caissier";
    var count = 0;
    for (int i = 0; i< employeeNames.length; i++) {
        String currentEmployees = employeeNames[i];
        String currentJob = positions[i];

        if (searchPosition.equals(currentJob)) {
            System.out.println(currentEmployees);
            count ++;
        }
    }
    if (count == 0) {
        System.out.println("Aucun employé trouvé");
    }
}